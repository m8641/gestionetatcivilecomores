package org.mairie.comores.dao;

import static org.assertj.core.api.Assertions.assertThat;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mairie.comores.entities.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class PersonneDaoTest {

	@Autowired
	private TestEntityManager entityManager;
	@Autowired
	private PersonneRepository personneRepository;
	// private static final String API_ROOT = "http://localhost:8081/api/books";
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");

	Date date1 = null;

	@Test
	public void findPersonneByIdtest() throws ParseException {

		date1 = dateFormat.parse("15/04/1981");
		Personne pers1 = new Personne("kassim", "kassim@yahoo.fr", date1);
		Personne pers2 = new Personne("bernar", "bernar@yahoo.fr", date1);
		Personne pers3 = new Personne("duboit", "bernar@yahoo.fr", date1);
		entityManager.persist(pers1);
		entityManager.persist(pers2);
		entityManager.persist(pers3);
		List<Personne> personnes = personneRepository.findAll();
		assertThat(personnes).hasSize(3).contains(pers1, pers2, pers3);

	}

	@Test
	public void createPersonneTest() throws ParseException {

		date1 = dateFormat.parse("01/05/1963");
		Personne personne = new Personne();
		personne.setName("Abdoul Madjid");
		personne.setEmail("wachehi@yahoo.fr");
		personne.setDatenaissance(date1);
		entityManager.persist(personne);

		Personne pers = personneRepository.findById(personne.getIdPers()).get();
		assertThat(pers).isEqualTo(personne);
	}

	@Test
	public void createPersonnesEmployeTest() throws ParseException {
		date1 = dateFormat.parse("01/05/1969");

		Personne pers1 = new Personne("Nayel", "nayel@gmail.com", date1);
		personneRepository.save(pers1);
		assertThat(pers1).hasFieldOrPropertyWithValue("name", "Nayel");
		assertThat(pers1).hasFieldOrPropertyWithValue("email", "nayel@gmail.com");
		assertThat(pers1).hasFieldOrPropertyWithValue("datenaissance", date1);

	}

	@Test
	public void deletePersonneTest() throws ParseException {
		date1 = dateFormat.parse("15/04/1981");
		Personne pers1 = new Personne("kassim", "kassim@yahoo.fr", date1);
		Personne pers2 = new Personne("bernard", "bernar@yahoo.fr", date1);
		Personne pers3 = new Personne("duboit", "bernar@yahoo.fr", date1);
		entityManager.persist(pers1);
		entityManager.persist(pers2);
		entityManager.persist(pers3);

		personneRepository.delete(pers1);

		Iterable<Personne> personnes = personneRepository.findAll();
		assertThat(personnes).hasSize(2).contains(pers2, pers3);

	}

	@Test
	public void updatePersonneTest() throws ParseException {
		date1 = dateFormat.parse("15/04/1981");
		Personne pers1 = new Personne("kassim", "kassim@yahoo.fr", date1);
		entityManager.persist(pers1);

		Personne pers = personneRepository.findById(pers1.getIdPers()).get();
		pers.setName("baba");
		personneRepository.save(pers);
		Personne persUp = personneRepository.findById(pers1.getIdPers()).get();

		assertThat(persUp).hasFieldOrPropertyWithValue("name", "baba");
	}

}
