package org.mairie.comores.metier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mairie.comores.dao.PersonneRepository;
import org.mairie.comores.entities.Personne;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
public class PersonneMetierImplTest {

	@MockBean
	private PersonneRepository persRepository;

	@Autowired
	private IPersonneMetier service;

	SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	Date d1 = null;

	@TestConfiguration
	static class PersonServiceConfig {
		@Bean
		public IPersonneMetier personService() {
			return new PersonneMetierImpl();
		}
	}

	@Test
	public void listePersonneTest() throws ParseException {

		d1 = format.parse("15/03/2021");

		List<Personne> ListPers = Arrays.asList(new Personne("combo", "combo@yahoo.fr", d1),
				new Personne("merinagnac", "merinagnac@yahoo.fr", d1));
		when(service.listePersonne()).thenReturn(ListPers);
		List<Personne> personnes = service.listePersonne();

		// assert
		Personne p1 = ListPers.get(0);
		Personne p2 = ListPers.get(1);

		assertEquals(2, personnes.size());
		assertThat(personnes.contains(p1));
		assertThat(personnes.contains(p2));

	}

	@Test
	public void getPersonneIdTest() throws ParseException {

		d1 = format.parse("15/03/2021");

		Personne pers = new Personne();
		pers.setIdPers(1L);
		pers.setName("seimour");
		pers.setEmail("seimour@gmail.com");
		pers.setDatenaissance(d1);
		when(persRepository.findById(pers.getIdPers())).thenReturn(Optional.of(pers));

		// Act
		Personne p = service.getPersonne(pers.getIdPers());
		// Assert
		verify(persRepository, times(1)).findById(pers.getIdPers());
		assertEquals(pers.getIdPers(), p.getIdPers());
		assertEquals(pers.getName(), p.getName());
		assertEquals(pers.getEmail(), p.getEmail());
		assertEquals(pers.getDatenaissance(), p.getDatenaissance());

	}

	@Test
	public void createPersonneTest() throws ParseException {

		d1 = format.parse("15/03/2021");
		Personne pers = new Personne();
		pers.setIdPers(1L);
		pers.setName("seimour");
		pers.setEmail("seimour@gmail.com");
		pers.setDatenaissance(d1);

		service.savePersonne(pers);
		verify(persRepository, times(1)).save(pers);

	}

	@Test
	public void deletePersonneTest() throws ParseException {

		d1 = format.parse("15/03/2021");
		Personne pers = new Personne();
		pers.setIdPers(3L);
		pers.setName("seimour");
		pers.setEmail("seimour@gmail.com");
		pers.setDatenaissance(d1);
		service.savePersonne(pers);
		// Act
		service.deletePersonne(3L);
		// Assert
		verify(persRepository, times(1)).deleteById(pers.getIdPers());

	}

}
