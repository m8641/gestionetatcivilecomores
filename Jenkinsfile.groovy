node{
  try{
   def buildNum = env.BUILD_NUMBER
   def branchName = env.BRANCH_NAME

   print buildNum
   print branchName

    stage('Env - clone generator'){
       git "https://gitlab.com/m8641/generator.git"
    }

   stage('Env - run  Mysql'){
     sh " docker-compose -f docker-compose-mysql.yml up -d"
     sh "docker ps -a"
     }

  /*Recuperation du dépôt git applicatif */
  stage('SERVICE - Git checkout'){
    git branch: branchName, url: "https://gitlab.com/m8641/gestionetatcivilecomores.git"
  }
  /* déterminer l'extension */
  if(branchName == "dev"){
   extension = "-SNAPSHOT"
  }
  if(branchName == "stage"){
   extension = "-RC"
  }
  if(branchName == "master"){
   extension = ""
  }
  
  /* Récuperation du commitID long */
  def commitIdLong = sh returnStdout: true, script: 'git rev-parse HEAD'
  
  /* Récuperation du commitID court */
  def commitId = commitIdLong.take(7)  
  
  /* Modification de la version dans le pom.xml */
   sh "sed -i s/'-XXX'/${extension}/g pom.xml"
   
  /* Récuperation de la version du pom.xml après modification */
   def version = sh returnStdout: true, script: "cat pom.xml | grep -A1 '<artifactId>GestionMairieComores'| tail -1 |perl -nle 'm{.*<version>(.*)</version>.*};print \$1'| tr -d '\n'"

  print """
  #################################################
    BrancheName:$branchName
	CommitID:$commitId
	AppVersion:$version
	JobNumber:$buildNum
  ##################################################
   """
  } finally {
    sh 'docker rm -f mysql_serveur'
    cleanWs()
  }
}
